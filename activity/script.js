console.log('hello world!');

//creating an object constructor instead of object literals

// function Pokemon(name, level) {
// 	//properties
// 	this.name = name;
// 	this.level = level;
// 	this.health = 2 * level;
// 	this.attack = 2 * level;

// 	//methods
// 	this.tackle = function(target) {
// 		console.log(`${this.name} tackled ${target.name}`);
// 		console.log("targetPokemon's health is now reduced.")
// 	};
// 	this.faint = function() {
// 		console.log(`${this.name} fainted.`)
// 	}
// }

// //Create new instances
// let pikachu = new Pokemon("Pikachu", 16);
// let rattata = new Pokemon("Rattata", 8);

// pikachu.tackle(rattata);
// rattata.tackle(pikachu);


// rattata.faint();

let trainer = {
	Name:'Ash Ketchum',
	Age: 10,
	Pokemon:['Pikachu','Butterfree','Pidgeot','Bulbasaur','Charizard','Squirtle'],
	friends:{ kanto: [`Clemont`,`Professor Oak`],
		palletTown: [`"Serena"`, `Max`],
		pewterCity: [`Brock`,`Misty`],
		hoenn: [`May`, `Max`] 
		},

	talk: function(){
		console.log(`Pikachu! I choose you!`);
    }
}
   console.log(trainer);

   console.log(`Welcome trainer ${trainer.Name}! You are ${trainer['Age']} years old, and makes you go for a gym battle and earn a badge. Your roster is ${trainer.Pokemon.join(",")}.`);

   console.log(`Result of dot notation`);

   console.log(trainer.Name);

   console.log('trainers One and Only Forever ' + trainer.friends.palletTown[0]);
   console.log('trainers friend ' + trainer.friends.pewterCity[0]);
   console.log('trainers friend ' + trainer.friends.hoenn[0]);
   console.log('trainers professor ' + trainer.friends.kanto[1]);

   console.log(`Result of bracket notation:`);
    
   console.log(trainer["Pokemon"]);

   console.log(`Result of talk method`);

   trainer.talk();

   function Pokemon(name, lvl){
	this.name = name;
	this.level = lvl;
	this.health = parseInt(lvl * 50);
	this.attack = parseInt((lvl * 50)/3);
	//this.health = parseInt(lvl * 50);
	//this.attack = parseInt((lvl * 50)/3);
	this.health = lvl * 2;
	this.attack = lvl;
	this.tackle = function(rival){
		rival.health -= parseInt(this.attack);
		//console.log(rival.health);
		console.log(`${this.name} make ${this.attack} damage to ${rival.name}. Remaining health is ${rival.health}.`);
		console.log(`${this.name} tackled ${rival.name}.`);
		console.log(`${rival.name}'s health is now reduced to ${rival.health}.`);

		if (rival.health <= 0){
			rival.faint();	
		}
		return rival;
		//return rival;
	},
	this.faint = function(){
		console.log(`target pokemon ${name} has fainted!`);
  }
}

//instantiate
let butterfree = new Pokemon(`pidgeot`,5);
let charmander = new Pokemon(`Char`, 6);
let squirtle = new Pokemon(`piplup`, 10);

console.log(butterfree);
console.log(charmander);
console.log(squirtle);

squirtle.tackle(butterfree);
// let butterfree = new Pokemon(`pidgeot`,5);
// let charmander = new Pokemon(`Char`, 6);
// let squirtle = new Pokemon(`piplup`, 10);
let pikachu = new Pokemon(`Pikachu`,20);
let geodude = new Pokemon(`Geodude`,8);
let mewtwo = new Pokemon(`Mewtwo`,100);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu);
mewtwo.tackle(pikachu);
console.log(pikachu);
console.log(geodude);
geodude.tackle(mewtwo);
geodude.tackle(mewtwo);
geodude.tackle(mewtwo);
geodude.tackle(mewtwo);
geodude.tackle(mewtwo);
geodude.tackle(mewtwo);
geodude.tackle(mewtwo);
mewtwo.tackle(geodude);